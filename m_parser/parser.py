from anytree import Node
from m_parser.firsts import firsts
from m_parser.follows import follows
from m_parser.nt_states import nt_states
from m_parser.productions import productions
from m_scanner.scanner import Scanner


class Parser:
    EPSILON = 'EPSILON'

    def __init__(self, get_next_token):
        self.get_next_token = get_next_token
        self.productions = productions
        self.nt_states = nt_states
        self.firsts = firsts
        self.follows = follows

        self.nodes_path_to_root_stack = []
        self.root_node = Node(self.nt_states[0])
        self.nodes_path_to_root_stack.append(self.root_node)

        self.lookahead = None
        self.current_token_of_lookahead = None
        self.errors = []

        self.parsing_is_finished = False

    def next_lookahead(self):
        next_token = self.get_next_token()
        self.current_token_of_lookahead = next_token
        if not next_token['token'] == Scanner.DOLLAR_SIGN_TOKEN:
            self.lookahead = next_token['token']['label']
        else:
            self.lookahead = Scanner.DOLLAR_SIGN_TOKEN

    def build_tree(self):
        self.next_lookahead()

        while not self.parsing_is_finished and self.nodes_path_to_root_stack:
            self.build_last_product_by_lookahead()

    def build_last_product_by_lookahead(self):

        last_state_in_stack = self.nodes_path_to_root_stack[0].name

        if last_state_in_stack == '$':
            print(' ')

        if not last_state_in_stack in self.nt_states:
            self.match_terminal_states()
            return

        our_current_firsts = self.firsts[last_state_in_stack]
        our_current_follows = self.follows[last_state_in_stack]

        if our_current_firsts.__contains__(self.lookahead):
            performed_production = False
            for production in self.productions[last_state_in_stack]:
                for state in production:
                    if state == self.lookahead:
                        self.perform_production(production)
                        break
                    if not state in self.nt_states:
                        break
                    if firsts[state].__contains__(self.lookahead):
                        self.perform_production(production)
                        break
                    elif firsts[state].__contains__(self.EPSILON):
                        continue
                    else:
                        break
                if performed_production:
                    break

        elif our_current_follows.__contains__(self.lookahead):
            if not self.firsts[last_state_in_stack].__contains__(self.EPSILON):
                error = {
                    'token': self.current_token_of_lookahead,
                    'message': 'syntax error, missing ' + self.nodes_path_to_root_stack[0].name
                }
                self.errors.append(error)
                missed_node = self.nodes_path_to_root_stack.pop(0)
                missed_node.parent = None
            else:
                epsilon_production = None
                for production in self.productions[last_state_in_stack]:
                    # we want to check that all states of this production can be epsilon or not
                    can_be_epsilon = True
                    if production.__len__() == 1 and production[0] == self.EPSILON:
                        epsilon_production = production
                        break
                    for state in production:
                        if not state in self.nt_states or not self.firsts[state].__contains__(self.EPSILON):
                            can_be_epsilon = False
                            break
                    if can_be_epsilon:
                        epsilon_production = production
                        break
                self.perform_production(epsilon_production)

        else:
            if self.lookahead == Scanner.DOLLAR_SIGN_TOKEN:
                error = {
                    'token': self.current_token_of_lookahead,
                    'message': 'syntax error, unexpected EOF'
                }
                self.errors.append(error)
                self.parsing_is_finished = True
                for not_matched_state in self.nodes_path_to_root_stack:
                    not_matched_state.parent = None
                return

            # bad token - we will delete this lookahead
            error = {
                'token': self.current_token_of_lookahead,
                'message': 'syntax error, illegal ' + self.lookahead
            }
            self.errors.append(error)
            self.next_lookahead()

    def perform_production(self, selected_production):
        parent_node = self.nodes_path_to_root_stack.pop(0)

        new_nodes = []
        for state in (selected_production):
            new_node = Node(name=state, parent=parent_node)
            new_nodes.append(new_node)
        for new_node in list(reversed(new_nodes)):
            if new_node.name == self.EPSILON:
                new_node.name = self.EPSILON.lower()
            else:
                self.nodes_path_to_root_stack.insert(0, new_node)

    def match_terminal_states(self):
        matched_terminal_node = self.nodes_path_to_root_stack[0]
        if matched_terminal_node.name == self.lookahead:

            if not matched_terminal_node.name == Scanner.DOLLAR_SIGN_TOKEN:
                matched_terminal_node.name = '(' + self.current_token_of_lookahead['token']['type'] + ', ' + \
                                             self.current_token_of_lookahead['matched_string'] + ') '

            self.nodes_path_to_root_stack.pop(0)
        else:
            error = {
                'token': self.current_token_of_lookahead,
                'message': 'syntax error, missing ' + matched_terminal_node.name
            }
            matched_terminal_node.parent = None
            self.errors.append(error)
            self.nodes_path_to_root_stack.pop(0)
            return

        if self.lookahead == Scanner.DOLLAR_SIGN_TOKEN:
            self.parsing_is_finished = True
            return

        self.next_lookahead()
