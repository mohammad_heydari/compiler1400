default_symbols = [
    'if',
    'else',
    'void',
    'int',
    'while',
    'break',
    'switch',
    'default',
    'case',
    'return',
    'for',
]
