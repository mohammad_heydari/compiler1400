tokens = [
    {
        'regex': 'if',
        'label': 'if',
        'type': 'KEYWORD',
        'valid': True
    },
    {
        'regex': 'else',
        'label': 'else',
        'type': 'KEYWORD',
        'valid': True
    },
    {
        'regex': 'void',
        'label': 'void',
        'type': 'KEYWORD',
        'valid': True
    },
    {
        'regex': 'int',
        'label': 'int',
        'type': 'KEYWORD',
        'valid': True
    },
    {
        'regex': 'while',
        'label': 'while',
        'type': 'KEYWORD',
        'valid': True
    },
    {
        'regex': 'break',
        'label': 'break',
        'type': 'KEYWORD',
        'valid': True
    },
    {
        'regex': 'switch',
        'label': 'switch',
        'type': 'KEYWORD',
        'valid': True
    },
    {
        'regex': 'default',
        'label': 'default',
        'type': 'KEYWORD',
        'valid': True
    },
    {
        'regex': 'case',
        'label': 'case',
        'type': 'KEYWORD',
        'valid': True
    },
    {
        'regex': 'return',
        'label': 'return',
        'type': 'KEYWORD',
        'valid': True
    },
    {
        'regex': 'for',
        'label': 'for',
        'type': 'KEYWORD',
        'valid': True
    },
    {
        'regex': ';',
        'label': ';',
        'type': 'SYMBOL',
        'valid': True
    },
    {
        'regex': ':',
        'label': ':',
        'type': 'SYMBOL',
        'valid': True
    },
    {
        'regex': ',',
        'label': ',',
        'type': 'SYMBOL',
        'valid': True
    },
    {
        'regex': '\[',
        'label': '[',
        'type': 'SYMBOL',
        'valid': True
    },
    {
        'regex': '\]',
        'label': ']',
        'type': 'SYMBOL',
        'valid': True
    },
    {
        'regex': '\(',
        'label': '(',
        'type': 'SYMBOL',
        'valid': True
    },
    {
        'regex': '\)',
        'label': ')',
        'type': 'SYMBOL',
        'valid': True
    },
    {
        'regex': '{',
        'label': '{',
        'type': 'SYMBOL',
        'valid': True
    },
    {
        'regex': '}',
        'label': '}',
        'type': 'SYMBOL',
        'valid': True
    },
    {
        'regex': '\+',
        'label': '+',
        'type': 'SYMBOL',
        'valid': True
    },
    {
        'regex': '-',
        'label': '-',
        'type': 'SYMBOL',
        'valid': True
    },
    {
        'regex': '\*',
        'label': '*',
        'type': 'SYMBOL',
        'valid': True
    },
    {
        'regex': '=',
        'label': '=',
        'type': 'SYMBOL',
        'valid': True
    },
    {
        'regex': '==',
        'label': '==',
        'type': 'SYMBOL',
        'valid': True
    },
    {
        'regex': '<',
        'label': '<',
        'type': 'SYMBOL',
        'valid': True
    },
    {
        'regex': '//((?!//).)*\n',
        'label': 'inline_comment',
        'type': 'COMMENT',
        'valid': True
    },
    {
        'regex': '/\*((?!\*/).)*\*/',
        'label': 'large_comment',
        'type': 'COMMENT',
        'valid': True
    },
    {
        'regex': '\t|\n|\r|\t|\v|\f| ',
        'label': 'WHITESPACE',
        'type': 'WHITESPACE',
        'valid': True
    },
    {
        'regex': '[0-9]+',
        'label': 'NUM',
        'type': 'NUM',
        'valid': True
    },
    {
        'regex': '[A-Za-z][A-Za-z0-9]*',
        'label': 'ID',
        'type': 'ID',
        'valid': True
    },
    {
        'regex': '[0-9]+[A-Za-z]',
        'label': 'bad_number',
        'type': 'BAD_NUMBER',
        'valid': False,
        'message': 'Invalid number'
    },
    {
        'regex': '[A-Za-z0-9]+[^0-9a-zA-Z\+-_\(\){}\[\]\s]+',
        'label': 'invalid_input',
        'type': 'INVALID_INPUT',
        'valid': False,
        'message': 'Invalid input'
    },
    {
        'regex': '[\S]',
        'label': 'INVALID_INPUT',
        'type': 'INVALID_INPUT',
        'valid': False,
        'message': 'Invalid input'
    },
    {
        'regex': '/\*((?!\*/).)*',
        'label': 'unclosed_comment',
        'type': 'UNCLOSED_COMMENT',
        'valid': False,
        'message': 'Unclosed comment'
    },
    {
        'regex': '\*/',
        'label': 'unmatched_comment',
        'type': 'UNMATCHED_COMMENT',
        'valid': False,
        'message': 'Unmatched comment'
    },
]
