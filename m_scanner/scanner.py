import re


class Scanner:
    DOLLAR_SIGN_TOKEN = '$'
    WHITESPACE = 'WHITESPACE'
    UNCLOSED_COMMENT = 'UNCLOSED_COMMENT'
    COMMENT = 'COMMENT'

    def update_line_char_no(self):
        scanned_string_len = self._full_text.__len__() - self._reminded_text.__len__()
        scanned_string = self._full_text[:scanned_string_len]
        all_next_lines = [m.start() for m in re.finditer('\n', scanned_string)]
        if not all_next_lines:
            self._current_char_no = scanned_string.__len__() + 1
        else:
            self._current_line_no = all_next_lines.__len__() + 1
            self._current_char_no = scanned_string_len - all_next_lines[-1] + 1

    def __init__(self, file_path):
        # initializing object
        self._file_path = file_path
        self.file = open(file_path, 'r')
        self._full_text = self.file.read()
        self._reminded_text = self._full_text

        self._current_line_no = 1
        self._current_char_no = 1
        self._in_error_state = False

        self._errors = []

        # reading tokens
        from . import tokens as tokens_file
        self._tokens_by_label = {token['label']: token for token in tokens_file.tokens}

    def get_next_token(self):
        if self._reminded_text.__len__() == 0:
            next_matched_token = {
                'token': self.DOLLAR_SIGN_TOKEN,
                'matched_string': 'EOF',
                'line_no': self._current_line_no,
                'char_no': self._current_char_no
            }

            return next_matched_token

        all_matched_tokens = {}

        for token in self._tokens_by_label.values():

            regex = '^' + token['regex']
            regex_matches = re.search(regex, self._reminded_text)
            if regex_matches and regex_matches.regs[0][0] == 0:
                matched_token_label = token['label']
                all_matched_tokens[matched_token_label] = regex_matches.regs[0][1]

        if all_matched_tokens:
            best_matched_token = max(all_matched_tokens.items(), key=lambda matched_token_item: matched_token_item[1])

            matched_len = best_matched_token[1]
            token = self._tokens_by_label[best_matched_token[0]]
            matched_string = self._reminded_text[:matched_len]

            if matched_len != 0:
                if token['valid']:

                    self._reminded_text = self._reminded_text[matched_len:]
                    self.update_line_char_no()

                    if token['label'] == self.WHITESPACE or token['type'] == self.COMMENT:
                        return self.get_next_token()

                    next_matched_token = {
                        'token': token,
                        'matched_string': matched_string,
                        'line_no': self._current_line_no,
                        'char_no': self._current_char_no
                    }

                    return next_matched_token

                else:

                    if token['type'] == self.UNCLOSED_COMMENT:
                        matched_string = matched_string[:7] + '...'

                    error = {
                        'text': self._reminded_text[:20],
                        'message': token['message'],
                        'token': token,
                        'matched_string': matched_string,
                        'line_no': self._current_line_no
                    }

                    self._errors.append(error)

                    self._reminded_text = self._reminded_text[matched_len:]

                    if token['type'] == self.UNCLOSED_COMMENT:
                        next_matched_token = {
                            'token': self.DOLLAR_SIGN_TOKEN,
                            'matched_string': 'EOF',
                            'line_no': self._current_line_no,
                            'char_no': self._current_char_no
                        }
                        return next_matched_token

        return self.get_next_token()
