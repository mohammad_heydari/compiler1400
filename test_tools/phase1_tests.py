from test_tools.one_test import do_one_test

test_options = [
    {
        'file_name': 'tokens.txt',
        'name': 'Tokens'
    },
    {
        'file_name': 'symbol_table.txt',
        'name': 'Symbols'
    },
    {
        'file_name': 'lexical_errors.txt',
        'name': 'Error reporting'
    }
]
for i in range(2, 3):
    input_dir = '../PA1_sample_programs/PA1_sample_programs/T0' + str(i) + '/'
    do_one_test(test_dir=input_dir, test_options=test_options)
