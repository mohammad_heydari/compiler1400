from test_tools.one_test import do_one_test

test_options = [
    {
        'file_name': 'syntax_errors.txt',
        'name': 'Error reporting'
    },
    {
        'file_name': 'parse_tree.txt',
        'name': 'Parsing'
    }
]
for i in range(1, 11):
    input_dir = '../PA2_Resources/PA2_Resources/T' + str(i) + '/'
    do_one_test(test_dir=input_dir, test_options=test_options)
