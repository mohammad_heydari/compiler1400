from compiler import MCompiler

CRED = '\33[31m'
CGREEN = '\33[32m'
CBLUE = '\33[34m'


def do_one_test(test_dir, test_options):
    compiler = MCompiler(test_dir + 'input.txt')
    compiler.compile()
    print(CBLUE + 'Testing ' + test_dir)
    for test in test_options:
        file_name = test['file_name']
        test_name = test['name']

        import spacy

        expected = open(test_dir + file_name, 'r').read()
        output = open(file_name, 'r').read()

        if expected == output:
            print(CGREEN + ' ' + test_name + ' was successful.')
        else:

            nlp = spacy.load('en_core_web_sm')
            doc1 = nlp(expected)
            doc2 = nlp(output)

            similarity = doc1.similarity(doc2)

            print(CRED + ' ' + test_name + ' failed. ' + str(similarity))
