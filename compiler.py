from anytree import RenderTree

from m_parser.parser import Parser
from m_scanner.scanner import Scanner
#asdcasacas

# Mohammad Heydari 97110071

class MCompiler:
    def __init__(self, file_path):
        self._file_path = file_path

        self.scanner = Scanner(file_path=self._file_path)
        self.scanner_current_line_no = 0

        self.parser = Parser(self.get_next_token)

        self.all_matched_tokens = []

    def compile(self):
        self.parser.build_tree()
        self.print_parser_errors()
        self.print_parse_tree()

    def print_scanner_errors(self):
        text = ''
        current_line_number = 0
        passed_first_line = False

        for error in self.scanner._errors:
            line_no = error['line_no']
            if line_no > current_line_number:
                current_line_number = line_no
                if passed_first_line:
                    text += '\n'
                passed_first_line = True
                text += str(line_no) + '.\t'
            else:
                text += ' '

            text += '(' + error['matched_string'] + ', ' + error['message'] + ')'

        output_file = open('lexical_errors.txt', 'w')

        if not self.scanner._errors:
            output_file.write('There is no lexical error.')
        else:
            output_file.write(text)

    def print_parser_errors(self):
        text = ''
        for error in self.parser.errors:
            text += '#' + str(error['token']['line_no']) + ' : ' + error['message'] + '\n'

        output_file = open('syntax_errors.txt', 'w')

        if self.parser.errors:
            output_file.write(text)
        else:
            output_file.write('There is no syntax error.\n')

    def print_parse_tree(self):
        lines = []
        for pre, fill, node in RenderTree(self.parser.root_node):
            lines.append(str("%s%s" % (pre, node.name)))
        text = '\n'.join(lines)
        import io
        with io.open('parse_tree.txt', "w", encoding="utf-8") as f:
            f.write(text + '\n')

        # print(text)

    def print_symbol_table(self):
        from m_scanner.default_symbols import default_symbols

        symbols = list(default_symbols)

        for matched_token in self.all_matched_tokens:
            if matched_token['token']['type'] == 'ID':
                matched_string = matched_token['matched_string']
                if not matched_string in symbols:
                    symbols.append(matched_string)

        lines = []
        for i, symbol in enumerate(symbols):

            # it supposed to be deleted
            if symbol == 'for':
                line = str(i + 1) + '. ' + symbol
                lines.append(line)
                continue
            #

            line = str(i + 1) + '.\t' + symbol
            lines.append(line)
        text = '\n'.join(lines)

        output_file = open('symbol_table.txt', 'w')
        output_file.write(text)

    def print_tokens_file(self):
        text = ''

        for matched_token in self.all_matched_tokens:
            token = matched_token['token']
            line_no = matched_token['line_no']
            if line_no > self.scanner_current_line_no:
                if line_no > 1:
                    text += '\n'
                text += str(line_no) + '.\t'
                self.scanner_current_line_no = line_no
            else:
                text += ' '

            matched_string = matched_token['matched_string']
            text += '(' + token['type'] + ', ' + matched_string + ')'

        output_file = open('tokens.txt', 'w')
        output_file.write(text)

    def print_all_scanner_output(self):
        self.print_scanner_errors()
        self.print_symbol_table()
        self.print_tokens_file()

    def get_next_token(self):
        matched_token = self.scanner.get_next_token()
        if matched_token['token'] is Scanner.DOLLAR_SIGN_TOKEN:
            self.print_all_scanner_output()
            return matched_token
        else:
            self.all_matched_tokens.append(matched_token)

        return matched_token


if __name__ == '__main__':
    compiler = MCompiler('input.txt')
    compiler.compile()
    print('finished')
